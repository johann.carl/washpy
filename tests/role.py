from washpy.role import Role

import json


def test_role_parsing():
    """
    Data captured in the wild.
    """
    json_data = '{"ID": 102,"Description": "Default: Network and Cloud Management","Permissions": [8456,8452,8450,8449,8712,8708,8706,8705]}'
    data = json.loads(json_data)

    r = Role(**data)

    assert r.ID == 102
    assert r.Description == "Default: Network and Cloud Management"
    assert r.Permissions == [8456, 8452, 8450, 8449, 8712, 8708, 8706, 8705]
