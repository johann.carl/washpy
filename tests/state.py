from washpy.state import *


def test_StateModel_parsing():
    # real data, pExtended is a bit truncated
    json_data = {
        "Status": 5,
        "ProgramID": 1,
        "ProgramPhase": 260,
        "SyncState": 2,
        "pRemainingTime": "PT0H26M0S",
        "pElapsedTime": "PT0H34M0S",
        "pSystemTime": "2024-04-24T17:47:01",
        "pStartTime": "2024-04-24T17:13:09",
        "pEndTime": "2024-04-24T18:13:01",
        "pLastNotificationTime": "",
        "ProcessUID": "DC0DF9D0E11308F6182F6B0B9B5C4473A5212271461EB823CF0546A71A5A12F8",
        "pExtended": {
            "DoorOpen": False,
            "DeviceLocked": False,
            "StepName": "",
            "Extras": {
                "Quick": False,
            },
            "AutoDosing": {
                "Container": 0,
                "NoLaundryDetergent": False,
            },
        },
    }

    expected = StateModel(
        Status=Status(5),
        ProgramID=1,
        ProgramPhase=260,
        SyncState=2,
        pRemainingTime="PT0H26M0S",
        pElapsedTime="PT0H34M0S",
        pSystemTime="2024-04-24T17:47:01",
        pStartTime="2024-04-24T17:13:09",
        pEndTime="2024-04-24T18:13:01",
        pLastNotificationTime="",
        ProcessUID="DC0DF9D0E11308F6182F6B0B9B5C4473A5212271461EB823CF0546A71A5A12F8",
        pExtended={
            "DoorOpen": False,
            "DeviceLocked": False,
            "StepName": "",
            "Extras": {
                "Quick": False,
            },
            "AutoDosing": {
                "Container": 0,
                "NoLaundryDetergent": False,
            },
        },
    )

    assert expected == StateModel(**json_data)


def test_State_parsing():
    # real data, pExtended is a bit truncated
    json_data = {
        "Status": 5,
        "ProgramID": 1,
        "ProgramPhase": 260,
        "SyncState": 2,
        "pRemainingTime": "PT0H26M0S",
        "pElapsedTime": "PT0H34M0S",
        "pSystemTime": "2024-04-24T17:47:01",
        "pStartTime": "2024-04-24T17:13:09",
        "pEndTime": "2024-04-24T18:13:01",
        "pLastNotificationTime": "",
        "ProcessUID": "DC0DF9D0E11308F6182F6B0B9B5C4473A5212271461EB823CF0546A71A5A12F8",
        "pExtended": {
            "DoorOpen": False,
            "DeviceLocked": False,
            "StepName": "",
            "Extras": {
                "Quick": False,
            },
            "AutoDosing": {
                "Container": 0,
                "NoLaundryDetergent": False,
            },
        },
    }

    expected = State(
        Status=Status(5),
        ProgramID=1,
        ProgramPhase=260,
        SyncState=2,
        pRemainingTime="PT0H26M0S",
        pElapsedTime="PT0H34M0S",
        pSystemTime="2024-04-24T17:47:01",
        pStartTime="2024-04-24T17:13:09",
        pEndTime="2024-04-24T18:13:01",
        pLastNotificationTime="",
        ProcessUID="DC0DF9D0E11308F6182F6B0B9B5C4473A5212271461EB823CF0546A71A5A12F8",
        pExtended={
            "DoorOpen": False,
            "DeviceLocked": False,
            "StepName": "",
            "Extras": {
                "Quick": False,
            },
            "AutoDosing": {
                "Container": 0,
                "NoLaundryDetergent": False,
            },
        },
    )

    assert expected._model == State(**json_data)._model
