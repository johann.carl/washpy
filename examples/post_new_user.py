from washpy import DeviceUser
from washpy.user import User

d = DeviceUser(
    "https://192.168.1.251/Devices/000116343328", "Admin", "verySecurePassword!"
)

user = User(
    ID=102,
    LoginName="MyUser",
    Password="evenStrongerPassword!",
    Description="My first User",
    Roles=[1, 2],
)

# user the Admin standard user to add a new user
d.post_new_user(user)
